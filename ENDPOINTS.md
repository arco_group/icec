Endpoint numbering
==================

The following is a list of official endpoints and its type identifier:

* 1: TCP Endpoint
* 3: UDP Endpoint

* 11: FIFO Endpoint (Unix named pipes)
* 12: Serial Endpoint
* 13: XBee Endpoint
* 14: RRP Endpoint (RS-485)
* 15: nRF24 Endpoint
