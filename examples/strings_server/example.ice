// -*- mode: c++; coding: utf-8 -*-

module Example {
    interface Hello {
	void sayHelloFrom(string name, int times);
    };
};
