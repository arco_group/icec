// -*- mode: c++; coding: utf-8 -*-

module Example {

    interface Thermostat {
	void setTemperature(float temp);
    };

};
