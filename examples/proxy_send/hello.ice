// -*- mode: c++; coding: utf-8 -*-

module Example {

  interface Speaker {
    void sayToSpeaker(Object* s, string msg);
    void say(string msg);
  };

};
