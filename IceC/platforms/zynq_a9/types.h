/* -*- mode: c; coding: utf-8 -*- */

#ifndef _ICEC_TYPES_H_
#define _ICEC_TYPES_H_

#ifndef NULL
#define NULL 0
#endif

#if !defined(__cplusplus) && !defined(true)
#define false 0
#define true 1
typedef uint8_t bool;
#endif

typedef uint8_t byte;

#endif  /* _ICEC_TYPES_H_ */
