/* -*- mode: c; coding: utf-8 -*- */

#ifndef _ICEC_TYPES_H_
#define _ICEC_TYPES_H_

#include <stdint.h>

#define SSIZE_TYPE
typedef uint16_t ssize_t;

#define BOOL_TYPE
typedef uint8_t bool;
#define false 0
#define true 1

#define BYTE_TYPE
typedef uint8_t byte;

#endif  /* _ICEC_TYPES_H_ */
