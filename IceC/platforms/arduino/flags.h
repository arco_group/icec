// -*- mode: c++; coding: utf-8 -*-

#ifndef _ICEC_ARDUINO_FLAGS_H_
#define _ICEC_ARDUINO_FLAGS_H_

//
// NOTE: in Arduino IDE, there is not an easy way to define flags for
// compilation. Use this unit to do so.
//

// #define DEBUG 1
// #define DEBUG_LED 1
// #define DEBUG_LED_PIN 13
// #define LOGGER 1

#endif // _ICEC_ARDUINO_FLAGS_H_
