// -*- mode: c++; coding: utf-8 -*-

module Example {
    interface Led {
	void on();
	void off();
    };
};
