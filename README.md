Description
===========

Minimal implementation of [ZeroC Ice](http://zeroc.com) middleware in ANSI C.
See https://arcogroup.bitbucket.io/icec/ for more information.
